# hackerspace.gr website
## Overview

Repository for the [hackerspace.gr](https://hackerspace.gr) static webpage.

Built with [Hugo](https://gohugo.io/), using the [Coder Portfolio](https://github.com/naro143/hugo-coder-portfolio) theme.

## Installation

You need [Hugo](https://gohugo.io/getting-started/installing/) installed (version 0.77.0 or later).

Clone recursively 
```
git clone --recurse-submodules https://gitlab.com/hsgr/website.git
cd website
```

Run the following command to launch the website locally
```
hugo serve
````

## Contributing

If you want to contribute to this project, either by authoring code or by reporting bugs & issues, make sure to read the [Contribution Guidelines](CONTRIBUTING.md).

## License ##

[![license](https://img.shields.io/badge/license-CC%20BY--SA%204.0-6672D8.svg)](LICENSE)
