+++ 
date = "2023-11-17"
title = "Το Hackerspace.gr έχει νέο website!"
slug = "hackerspace-gr-new-website" 
+++

Κάναμε μια οργανωμένη προσπάθεια να βελτιώσουμε το site μας. Για το λόγο αυτό μεταβήκαμε στο Hugo και ανοίξαμε το repo δημόσια.

Μπορείτε να το βρείτε στο [Gitlab](https://gitlab.com/hsgr/website).
