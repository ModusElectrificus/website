+++
title = "Επικοινωνία"
slug = "contact"
thumbnail = "images/tn.png"
description = "Επικοινωνία"
+++

##### Θες να έρθεις σε επαφή μαζί μασ ή να ρωτήσεις κάτι για το hackerspace?

Βρες μας στα:
- Phone +30 213 0210 437
- [Mailing List](https://lists.hackerspace.gr/listinfo/discuss)
- [Matrix](https://riot.im/app/#/room/#hsgr:matrix.org)

<iframe width="100%" height="350" src="https://www.openstreetmap.org/export/embed.html?bbox=23.704510331153873%2C37.9980037068625%2C23.708281517028812%2C37.999998981445124&amp;layer=mapnik&amp;marker=37.9990839%2C23.7064918" style="border: 1px solid black"></iframe><br/><small><a href="https://www.openstreetmap.org/node/5483055953">Προβολή μεγαλύτερου χάρτη</a></small>