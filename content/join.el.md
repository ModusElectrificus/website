+++
title = "Συμμετοχή"
slug = "join"
thumbnail = "images/tn.png"
description = "Συμμετοχή"
+++

Το Hackerspace.gr είναι ανοιχτό σε όλους να αξιοποιήσουν τον χώρο και τον εξοπλισμό του. 

Έλα απ' τον χώρο και γίνε μέρος της ζωντανής του κοινότητας.

## Γίνε μέλος

Τα [μέλη](/members) μας είναι οι άνθρωποι που εξασφαλίζουν τη λειτουργία του, προσφέροντας μια σταθερή συνεισφορά.

**[Θες να γίνεις κι εσύ;](https://www.hackerspace.gr/wiki/How_we_roll#.CE.94.CE.B9.CE.B1.CF.87.CE.B5.CE.B9.CF.81.CE.B9.CF.83.CF.84.CE.AD.CF.82_.28Operators.29)**

![](/images/hsgr_wall.jpg)

